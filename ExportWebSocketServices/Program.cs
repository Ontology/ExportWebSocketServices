﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using ImportExport_Module;
using System.IO;

namespace ExportWebSocketServices
{
    class Program
    {
        private static Globals globals = new Globals();

        private static OntologyModDBConnector dbReaderWebsocketServers;
        private static OntologyModDBConnector dbReaderWebsocketServerRels;
        private static OntologyModDBConnector dbReaderWebsocketServicesAtts;
        private static OntologyModDBConnector dbReaderWebsocketServicesRels;
        private static OntologyModDBConnector dbReaderWebsocketControllerSoftDevel;
        private static OntologyModDBConnector dbReaderWebsocketControllerView;
        private static OntologyModDBConnector dbReaderWebsocketViewRels;
        private static OntologyModDBConnector dbReaderClassRel;
        private static OntologyModDBConnector dbReaderClassAtt;
        private static OntologyModDBConnector dbReaderChannels;
        private static OntologyModDBConnector dbReaderModuleFunctions;

        private static exportImportItem exportImportItem = new exportImportItem();

        static void Main(string[] args)
        {
            Initialize();

            if (args[0].ToLower() == "e")
            {
                ExportItems(args[1]);
            }
            else if (args[0].ToLower() == "i")
            {
                ImportItems(args[1]);
            }
        }

        private static void ImportItems(string path)
        {
            exportImportItem importItem = new exportImportItem();
            using (var streamReader = new StreamReader(path))
            {
                var jsonString = streamReader.ReadToEnd();
                importItem = Newtonsoft.Json.JsonConvert.DeserializeObject<exportImportItem>(jsonString);

            }

            var dbWriter = new OntologyModDBConnector(globals);

            dbWriter.SaveClass(importItem.Classes);
            dbWriter.SaveRelationTypes(importItem.RelationTypes);
            dbWriter.SaveAttributeTypes(importItem.AttributeTypes);
            dbWriter.SaveObjects(importItem.Objects);
            dbWriter.SaveClassAtt(importItem.ClassAttributes);
            dbWriter.SaveClassRel(importItem.ClassRelations);
            dbWriter.SaveObjAtt(importItem.ObjAtts);
            dbWriter.SaveObjRel(importItem.ObjRels);

        }

        private static void ExportItems(string path)
        {
            var result = GetWebSocketServers();
            if (result.GUID == globals.LState_Error.GUID) return;

            result = GetWebServerRels();

            if (result.GUID == globals.LState_Error.GUID) return;

            result = GetWebServerRels();

            if (result.GUID == globals.LState_Error.GUID) return;

            result = GetWebsocketServicesRels();

            if (result.GUID == globals.LState_Error.GUID) return;

            result = GetControllerRels();

            if (result.GUID == globals.LState_Error.GUID) return;

            result = GetViewRels();

            if (result.GUID == globals.LState_Error.GUID) return;

            result = GetChannels();

            if (result.GUID == globals.LState_Error.GUID) return;

            result = GetModuleFunctions();

            if (result.GUID == globals.LState_Error.GUID) return;


            var classes = exportImportItem.Objects.GroupBy(obj => new { GUID = obj.GUID_Parent }).Select(obj => new clsOntologyItem
            {
                GUID = obj.Key.GUID
            }).ToList();

            classes.ForEach(cls =>
            {
                var classItem = dbReaderWebsocketControllerSoftDevel.GetOItem(cls.GUID, globals.Type_Class);

                cls.GUID = classItem.GUID;
                cls.Name = classItem.Name;
                cls.GUID_Parent = classItem.GUID_Parent;
                cls.Type = classItem.Type;
            });

            var searchClassRels = classes.Select(cls => new clsClassRel
            {
                ID_Class_Left = cls.GUID
            }).ToList();

            result = dbReaderClassRel.GetDataClassRel(searchClassRels);

            if (result.GUID == globals.LState_Error.GUID) return;

            var classRels = (from classRel in dbReaderClassRel.ClassRels
                             join clssLeft in classes on classRel.ID_Class_Left equals clssLeft.GUID
                             join classRight in classes on classRel.ID_Class_Right equals classRight.GUID
                             select classRel).ToList();

            exportImportItem.ClassRelations.AddRange(classRels);

            var searchClassAtts = classes.Select(cls => new clsClassAtt
            {
                ID_Class = cls.GUID
            }).ToList();

            result = dbReaderClassAtt.GetDataClassAtts(searchClassAtts);

            if (result.GUID == globals.LState_Error.GUID) return;

            exportImportItem.ClassAttributes.AddRange(dbReaderClassAtt.ClassAtts);

            classes.AddRange(classes);
            classes.Add(globals.Root);
            var systemClass = new clsOntologyItem
            {
                GUID = "665dd88b792e4256a27a68ee1e10ece6",
                Name = "System",
                GUID_Parent = globals.Root.GUID,
                Type = globals.Type_Class
            };

            classes.Add(systemClass);

            var moduleManagementClass = new clsOntologyItem
            {
                GUID = "b5224e7f7d0d4beebf8eb9a0eeba5747",
                Name = "Module-Management",
                GUID_Parent = systemClass.GUID,
                Type = globals.Type_Class
            };

            classes.Add(moduleManagementClass);

            var moduleClass = new clsOntologyItem
            {
                GUID = "aa616051e5214facabdbcbba6f8c6e73",
                Name = "Module",
                GUID_Parent = moduleManagementClass.GUID,
                Type = globals.Type_Class
            };

            classes.Add(moduleClass);

            exportImportItem.Classes = classes;
            if (result.GUID == globals.LState_Success.GUID)
            {
                using (var streamWriter = new StreamWriter(path))
                {
                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(exportImportItem);
                    streamWriter.Write(jsonString);
                }
            }
        }

        private static clsOntologyItem GetWebSocketServers()
        {
            
            var searchWebSocketServers = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = "a3e4f229b8c94932b6d83e1447b54561"
                }
            };

            var result = dbReaderWebsocketServers.GetDataObjects(searchWebSocketServers);

            if (result.GUID == globals.LState_Success.GUID)
            {
                exportImportItem.Objects.AddRange(dbReaderWebsocketServers.Objects1);
            }

            return result;
        }

       private static clsOntologyItem GetWebServerRels()
        {
            var searchWebSocketServices = dbReaderWebsocketServers.Objects1.Select(server => new clsObjectRel
            {
                ID_Object = server.GUID,
            }).ToList();

            var result = dbReaderWebsocketServerRels.GetDataObjectRel(searchWebSocketServices);

            if (result.GUID == globals.LState_Success.GUID)
            {
                var objects = dbReaderWebsocketServerRels.ObjectRels.GroupBy(objRel => new { GUID = objRel.ID_Other, Name = objRel.Name_Other, GUID_Parent = objRel.ID_Parent_Other }).Select(objRel => new clsOntologyItem
                {
                    GUID = objRel.Key.GUID,
                    Name = objRel.Key.Name,
                    GUID_Parent = objRel.Key.GUID_Parent,
                    Type = globals.Type_Object
                }).ToList();

                exportImportItem.Objects.AddRange(objects);

                var relationTypes = dbReaderWebsocketServerRels.ObjectRels.GroupBy(objRel => new { GUID = objRel.ID_RelationType, Name = objRel.Name_RelationType }).Select(objRel => new clsOntologyItem
                {
                    GUID = objRel.Key.GUID,
                    Name = objRel.Key.Name,
                    Type = globals.Type_RelationType
                }).ToList();

                exportImportItem.RelationTypes.AddRange(relationTypes);
                exportImportItem.ObjRels = new List<clsObjectRel>();
                exportImportItem.ObjRels.AddRange(dbReaderWebsocketServerRels.ObjectRels);
            }

            return result;
        }

        public static clsOntologyItem GetWebsocketServicesRels()
        {
            var services = dbReaderWebsocketServerRels.ObjectRels.Where(rel => rel.ID_Parent_Other == "053cc82f3a4c4aaf9cdc33d80edff4a1").GroupBy(rel => new { GUID = rel.ID_Other})
                .Select(rel => new clsOntologyItem { GUID = rel.Key.GUID }).ToList();

            var searchServicesAtts = services.Select(contr => new clsObjectAtt { ID_Object = contr.GUID }).ToList();

            var result = dbReaderWebsocketServicesAtts.GetDataObjectAtt(searchServicesAtts);

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var attributeTypes = dbReaderWebsocketServicesAtts.ObjAtts.GroupBy(objAtt => new { GUID = objAtt.ID_AttributeType, Name = objAtt.Name_AttributeType, GUID_Parent = objAtt.ID_DataType }).Select(objAtt => new clsOntologyItem
            {
                GUID = objAtt.Key.GUID,
                Name = objAtt.Key.Name,
                GUID_Parent = objAtt.Key.GUID_Parent,
                Type = globals.Type_AttributeType
            }).ToList();

            exportImportItem.AttributeTypes.AddRange(attributeTypes);

            exportImportItem.ObjAtts.AddRange(dbReaderWebsocketServicesAtts.ObjAtts);

            var searchServicesRel = services.Select(rel => new clsObjectRel
            {
                ID_Object = rel.GUID
            }).ToList();

            result = dbReaderWebsocketServicesRels.GetDataObjectRel(searchServicesRel);

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var objects = dbReaderWebsocketServicesRels.ObjectRels.GroupBy(rel => new { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other }).Select(rel => new clsOntologyItem
            {
                GUID = rel.Key.GUID,
                Name = rel.Key.Name,
                GUID_Parent = rel.Key.GUID_Parent,
                Type = globals.Type_Object
            }).ToList();


            exportImportItem.Objects.AddRange(objects);

            var relationTypes = dbReaderWebsocketServicesRels.ObjectRels.GroupBy(objRel => new { GUID = objRel.ID_RelationType, Name = objRel.Name_RelationType }).Select(objRel => new clsOntologyItem
            {
                GUID = objRel.Key.GUID,
                Name = objRel.Key.Name,
                Type = globals.Type_RelationType
            }).ToList();

            exportImportItem.RelationTypes.AddRange(relationTypes);
            exportImportItem.ObjRels.AddRange(dbReaderWebsocketServicesRels.ObjectRels);
            return result;
        }

        private static clsOntologyItem GetControllerRels()
        {
            var controllers = dbReaderWebsocketServicesRels.ObjectRels.Where(rel => rel.ID_Parent_Other == "fb32cde6174647f2b09eb4b907ad509c").GroupBy(rel => new { GUID = rel.ID_Other }).Select(rel => new clsOntologyItem
            {
                GUID = rel.Key.GUID
            }).ToList();

            var searchControllerRels = controllers.Select(contrl => new clsObjectRel
            {
                ID_Object = contrl.GUID,
                ID_RelationType = "3e104b75e01c48a0b04112908fd446a0"
            }).ToList();

            var result = dbReaderWebsocketControllerSoftDevel.GetDataObjectRel(searchControllerRels);

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var develop = dbReaderWebsocketControllerSoftDevel.ObjectRels.GroupBy(rel => new { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other }).Select(rel => new clsOntologyItem
            {
                GUID = rel.Key.GUID,
                Name = rel.Key.Name,
                GUID_Parent = rel.Key.GUID_Parent,
                Type = globals.Type_Object
            }).ToList();

            exportImportItem.Objects.AddRange(develop);

            var relationTypes = dbReaderWebsocketControllerSoftDevel.ObjectRels.GroupBy(objRel => new { GUID = objRel.ID_RelationType, Name = objRel.Name_RelationType }).Select(objRel => new clsOntologyItem
            {
                GUID = objRel.Key.GUID,
                Name = objRel.Key.Name,
                Type = globals.Type_RelationType
            }).ToList();

            exportImportItem.RelationTypes.AddRange(relationTypes);

            exportImportItem.ObjRels.AddRange(dbReaderWebsocketControllerSoftDevel.ObjectRels);

            var searchViews = controllers.Select(contrl => new clsObjectRel
            {
                ID_Other = contrl.GUID,
                ID_RelationType = "e07469d9766c443e85266d9c684f944f"
            }).ToList();

            result = dbReaderWebsocketControllerView.GetDataObjectRel(searchViews);

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var views = dbReaderWebsocketControllerView.ObjectRels.GroupBy(rel => new { GUID = rel.ID_Object, Name = rel.Name_Object, GUID_Parent = rel.ID_Parent_Object }).Select(rel => new clsOntologyItem
            {
                GUID = rel.Key.GUID,
                Name = rel.Key.Name,
                GUID_Parent = rel.Key.GUID_Parent,
                Type = globals.Type_Object
            }).ToList();

            exportImportItem.Objects.AddRange(views);

            relationTypes = dbReaderWebsocketControllerView.ObjectRels.GroupBy(objRel => new { GUID = objRel.ID_RelationType, Name = objRel.Name_RelationType }).Select(objRel => new clsOntologyItem
            {
                GUID = objRel.Key.GUID,
                Name = objRel.Key.Name,
                Type = globals.Type_RelationType
            }).ToList();

            exportImportItem.RelationTypes.AddRange(relationTypes);

            exportImportItem.ObjRels.AddRange(dbReaderWebsocketControllerView.ObjectRels);

            return result;
        }

        private static clsOntologyItem GetViewRels()
        {
            var views = dbReaderWebsocketControllerView.ObjectRels.GroupBy(rel => new { GUID = rel.ID_Object, Name = rel.Name_Object, GUID_Parent = rel.ID_Parent_Object }).Select(rel => new clsOntologyItem
            {
                GUID = rel.Key.GUID,
                Name = rel.Key.Name,
                GUID_Parent = rel.Key.GUID_Parent,
                Type = globals.Type_Object
            }).ToList();

            var searchViewRels = views.Select(view => new clsObjectRel
            {
                ID_Object = view.GUID,
                ID_Parent_Other = "dbadb971a838485ba4eedce6c477ac3c"
            }).ToList();

            searchViewRels.AddRange(views.Select(view => new clsObjectRel
            {
                ID_Object = view.GUID,
                ID_Parent_Other = "aa616051e5214facabdbcbba6f8c6e73"
            }));

            searchViewRels.AddRange(views.Select(view => new clsObjectRel
            {
                ID_Object = view.GUID,
                ID_Parent_Other = "8a894710e08c42c5b829ef4809830d33"
            }));

            searchViewRels.AddRange(views.Select(view => new clsObjectRel
            {
                ID_Object = view.GUID,
                ID_Parent_Other = "094d728d6efc463c85c72dcfed903c78"
            }));
            
            var result = dbReaderWebsocketViewRels.GetDataObjectRel(searchViewRels);

            if (result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var cmdlrsMdls = dbReaderWebsocketViewRels.ObjectRels.GroupBy(rel => new { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other }).Select(rel => new clsOntologyItem
            {
                GUID = rel.Key.GUID,
                Name = rel.Key.Name,
                GUID_Parent = rel.Key.GUID_Parent,
                Type = globals.Type_Object
            }).ToList();

            exportImportItem.Objects.AddRange(cmdlrsMdls);

            var relationTypes = dbReaderWebsocketViewRels.ObjectRels.GroupBy(objRel => new { GUID = objRel.ID_RelationType, Name = objRel.Name_RelationType }).Select(objRel => new clsOntologyItem
            {
                GUID = objRel.Key.GUID,
                Name = objRel.Key.Name,
                Type = globals.Type_RelationType
            }).ToList();

            exportImportItem.RelationTypes.AddRange(relationTypes);

            exportImportItem.ObjRels.AddRange(dbReaderWebsocketViewRels.ObjectRels);

            return result;
        }

        private static clsOntologyItem GetModuleFunctions()
        {
            var modules = dbReaderWebsocketViewRels.ObjectRels.Where(viewRel => viewRel.ID_Parent_Other == "aa616051e5214facabdbcbba6f8c6e73").Select(viewRel => new clsObjectRel
            {
                ID_Object = viewRel.ID_Other,
                ID_Parent_Other = "86b05e853ce24a56be3c80375a686a82"
            }).ToList();

            var result = dbReaderModuleFunctions.GetDataObjectRel(modules);

            if (result.GUID == globals.LState_Success.GUID)
            {
                var relationTypes = dbReaderModuleFunctions.ObjectRels.GroupBy(objRel => new { GUID = objRel.ID_RelationType, Name = objRel.Name_RelationType }).Select(objRel => new clsOntologyItem
                {
                    GUID = objRel.Key.GUID,
                    Name = objRel.Key.Name,
                    Type = globals.Type_RelationType
                }).ToList();

                exportImportItem.RelationTypes.AddRange(relationTypes);

                exportImportItem.ObjRels.AddRange(dbReaderModuleFunctions.ObjectRels);
            }

            return result;
        }

        private static clsOntologyItem GetChannels()
        {
            var searchChannels = new List<clsOntologyItem> { new clsOntologyItem
                {
                    GUID_Parent = "2c2c4448e72e40b0a4863f2c0998f94c"
                }
            };

            var result = dbReaderChannels.GetDataObjects(searchChannels);

            if (result.GUID == globals.LState_Error.GUID) return result;

            exportImportItem.Objects.AddRange(dbReaderChannels.Objects1);

            return result;
        }

        private static void Initialize()
        {

            dbReaderWebsocketServers = new OntologyModDBConnector(globals);
            dbReaderWebsocketServerRels = new OntologyModDBConnector(globals);
            dbReaderWebsocketServicesAtts = new OntologyModDBConnector(globals);
            dbReaderWebsocketServicesRels = new OntologyModDBConnector(globals);
            dbReaderWebsocketControllerSoftDevel = new OntologyModDBConnector(globals);
            dbReaderWebsocketControllerView = new OntologyModDBConnector(globals);
            dbReaderWebsocketViewRels = new OntologyModDBConnector(globals);
            dbReaderClassRel = new OntologyModDBConnector(globals);
            dbReaderClassAtt = new OntologyModDBConnector(globals);
            dbReaderChannels = new OntologyModDBConnector(globals);
            dbReaderModuleFunctions = new OntologyModDBConnector(globals);
        }
    }
}


