﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportWebSocketServices
{
    public class exportImportItem
    {
        private List<clsOntologyItem> objects = new List<clsOntologyItem>();
        public List<clsOntologyItem> Objects
        {
            get { return objects; }
            set
            {
                objects = value;
            }
        }

        private List<clsOntologyItem> classes = new List<clsOntologyItem>();
        public List<clsOntologyItem> Classes
        {
            get { return classes; }
            set
            {
                classes = value;
            }
        }

        private List<clsOntologyItem> relationTypes = new List<clsOntologyItem>();
        public List<clsOntologyItem> RelationTypes
        {
            get { return relationTypes; }
            set
            {
                relationTypes = value;
            }
        }

        private List<clsOntologyItem> attributeTypes = new List<clsOntologyItem>();
        public List<clsOntologyItem> AttributeTypes
        {
            get { return attributeTypes; }
            set
            {
                attributeTypes = value;
            }
        }

        private List<clsClassAtt> classAttributes = new List<clsClassAtt>();
        public List<clsClassAtt> ClassAttributes
        {
            get { return classAttributes; }
            set
            {
                classAttributes = value;
            }
        }

        private List<clsClassRel> classRelations = new List<clsClassRel>();
        public List<clsClassRel> ClassRelations
        {
            get { return classRelations; }
            set
            {
                classRelations = value;
            }
        }

        private List<clsObjectAtt> objAtts = new List<clsObjectAtt>();
        public List<clsObjectAtt> ObjAtts
        {
            get { return objAtts; }
            set
            {
                objAtts = value;
            }
        }

        private List<clsObjectRel> objRels = new List<clsObjectRel>();
        public List<clsObjectRel> ObjRels
        {
            get { return objRels; }
            set
            {
                objRels = value;
            }
        }

    }
}
